use super::Sorting;
use ansi_term::{Colour, Style};
use derive_builder::Builder;
use regex::Regex;

// pub struct Style(ansi_term::Style);
// pub struct Colour(ansi_term::Colour);

// impl Deref for Style {
//     type Target = ansi_term::Style;
//     fn deref(&self) -> &ansi_term::Style {
//         self as &ansi_term::Style
//     }
// }

#[derive(Debug, Builder, Clone)]
pub struct RunningConfig {
    pub max_depth: Option<usize>,
    pub indent_size: usize,
    pub sort: Option<Sorting>,
    pub exclude: Vec<Regex>,
}

/// A vector of tuples (lower_bound, colour)
pub type ColoringRules = Vec<(u64, Colour)>;

#[derive(Debug, Builder, Clone)]
pub struct Config {
    pub file_name_style: Style,
    pub dir_name_style: Style,
    pub size_style: Style,
    pub coloring_rules: ColoringRules,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            file_name_style: Style::new().fg(Colour::RGB(255, 255, 255)),
            dir_name_style: Style::new().bold(),
            size_style: Style::new().bold().dimmed(),
            coloring_rules: vec![
                (1024, Colour::Blue),
                (1024u64.pow(2), Colour::Green),
                (1024u64.pow(3), Colour::Yellow),
                (1024u64.pow(4), Colour::Red),
            ],
        }
    }
}
