use derive_builder::Builder;
use ptree::{Style, TreeItem};
use std::borrow::Cow;
use std::io;
use std::path::{Path, PathBuf};

use crate::config::{Config, RunningConfig};
use crate::size::format_size;

/// Data associated with a given node.
#[derive(Debug, Builder, Clone)]
pub struct Data<'a> {
    /// For directories it's the size of all the contained files
    /// for files it's just size
    pub size: u64,
    pub depth: usize,

    pub running_config: &'a RunningConfig,
    pub config: &'a Config,
}

#[derive(Debug, Clone)]
pub enum UsageNode<'a> {
    Dir(PathBuf, Data<'a>, Vec<UsageNode<'a>>),
    File(PathBuf, Data<'a>),
}

impl<'a> UsageNode<'a> {
    pub fn data(&self) -> &Data {
        match self {
            UsageNode::Dir(_, data, _) => data,
            UsageNode::File(_, data) => data,
        }
    }

    pub fn name(&self) -> String {
        self.path()
            .file_stem()
            .and_then(|p| p.to_str())
            .unwrap_or("")
            .to_string()
    }

    pub fn path(&self) -> &Path {
        match self {
            UsageNode::Dir(path, _, _) => path,
            UsageNode::File(path, _) => path,
        }
    }

    pub fn is_dir(&self) -> bool {
        match self {
            UsageNode::Dir(_, _, _) => true,
            _ => false,
        }
    }
}

impl<'a> TreeItem for UsageNode<'a> {
    type Child = Self;
    fn write_self<W: io::Write>(&self, f: &mut W, style: &Style) -> io::Result<()> {
        let path = self.path();
        let data = self.data();

        let file_name = path.to_string_lossy().to_string();

        let formatted_file_name = if self.is_dir() {
            data.config.dir_name_style.paint(file_name)
        } else {
            data.config.file_name_style.paint(file_name)
        };

        let formatted_file_name = format!("{}", formatted_file_name);

        let desc = format!(
            "{} : {}",
            formatted_file_name,
            format_size(data.size, data.config)
        );

        write!(f, "{}", style.paint(desc))
    }
    fn children(&self) -> Cow<[Self::Child]> {
        match self {
            UsageNode::Dir(_, data, children) => {
                if let Some(depth) = data.running_config.max_depth {
                    if depth == data.depth {
                        return Cow::from(vec![]);
                    }
                }

                Cow::from(children)
            }
            UsageNode::File(_, _) => Cow::from(vec![]),
        }
    }
}
