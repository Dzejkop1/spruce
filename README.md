[![Latest version](https://img.shields.io/crates/v/spruce.svg)](https://crates.io/crates/spruce)

# Spruce!
A fast, pretty and easy to use alternative to `du`.

![A screenshot](screenshots/s1.png)

# Installation
```
cargo install spruce
```

# Usage
```
spruce 0.3.0
Dzejkop <jakubtrad@gmail.com>
A command line utility for analyzing disk usage.

USAGE:
    spruce [OPTIONS] [--] [dir]

FLAGS:
    -h, --help
            Prints help information

    -V, --version
            Prints version information


OPTIONS:
    -d, --depth <depth>
            Max recursion depth

    -X, --exclude <exclude>...
            Exlucde files and directories with regular expressions

            For example, in order to exclude the .git directory use `-X "\.git"` You can use multiple patterns with a
            single flag, like this `-X "\.git" "target", or by using the flag many times `-X "\.git" -X target`
        --sort <sort>
            Sorting mode: "name" or "size"


ARGS:
    <dir>
            Target directory, if not specified will use the current working directory
```