use rayon::iter::ParallelBridge;
use rayon::prelude::*;
use regex::Regex;
use std::fs::{metadata, read_dir};
use std::path::{Path, PathBuf};
use std::str::{self, FromStr};
use structopt::StructOpt;

mod config;
mod size;
mod usage_tree;

use config::{Config, RunningConfig};
use ptree::print_tree;
use usage_tree::{DataBuilder, UsageNode};

fn get_usage<'a>(
    p: &Path,
    depth: usize,
    config: &'a Config,
    running_config: &'a RunningConfig,
) -> Option<UsageNode<'a>> {
    let entry_name: PathBuf = p.file_name()?.into();

    for ex in &running_config.exclude {
        if let Some(entry_name_str) = entry_name.to_str() {
            if ex.is_match(entry_name_str) {
                return None;
            }
        }
    }

    if p.is_dir() {
        let usage_entries = read_dir(p).ok()?;
        let mut usage_entries = usage_entries
            .par_bridge()
            .filter_map(Result::ok)
            .filter_map(|e| get_usage(&e.path(), depth + 1, config, running_config))
            .collect::<Vec<_>>();

        let data = DataBuilder::default()
            .size(usage_entries.iter().map(|e| e.data().size).sum())
            .depth(depth)
            .running_config(running_config)
            .config(config)
            .build()
            .ok()?;

        match running_config.sort {
            Some(Sorting::BySize) => usage_entries.sort_by_key(|x| -(x.data().size as isize)),
            Some(Sorting::ByName) => usage_entries.sort_by_key(|x| x.name()),
            _ => (),
        }

        Some(UsageNode::Dir(entry_name, data, usage_entries))
    } else {
        let data = metadata(p).ok()?;
        let data = DataBuilder::default()
            .size(data.len())
            .depth(depth)
            .running_config(running_config)
            .config(config)
            .build()
            .ok()?;

        Some(UsageNode::File(entry_name, data))
    }
}

#[derive(Debug, StructOpt)]
#[structopt(author, about)]
struct Opt {
    #[structopt(parse(from_os_str))]
    /// Target directory, if not specified will use the current working directory
    pub dir: Option<PathBuf>,

    #[structopt(short = "d", long = "depth")]
    /// Max recursion depth
    pub depth: Option<usize>,

    #[structopt(long = "sort")]
    /// Sorting mode: "name" or "size"
    pub sort: Option<Sorting>,

    #[structopt(short="X", long = "exclude")]
    /// Exlucde files and directories with regular expressions
    ///
    /// For example, in order to exclude the .git directory use `-X "\.git"`
    /// You can use multiple patterns with a single flag, like this `-X "\.git" "target",
    /// or by using the flag many times
    /// `-X "\.git" -X target`
    pub exclude: Vec<String>
}

#[derive(Clone, Copy, Debug, StructOpt)]
pub enum Sorting {
    ByName,
    BySize,
}

impl FromStr for Sorting {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "name" => Ok(Sorting::ByName),
            "size" => Ok(Sorting::BySize),
            _ => Err(String::from("Invalid sorting mode")),
        }
    }
}

fn run() -> Result<(), String> {
    let args = Opt::from_args();

    let exclusion_rules: Vec<Regex> = args
        .exclude
        .iter()
        .map(|s| Regex::from_str(&s))
        .collect::<Result<_, _>>()
        .map_err(|err| format!("Invalid regular expression in exclude: {}", err))?;

    let running_config = config::RunningConfigBuilder::default()
        .max_depth(args.depth)
        .indent_size(3)
        .sort(args.sort)
        .exclude(exclusion_rules)
        .build()
        .unwrap();

    let config = config::Config::default();

    let dir = match args.dir {
        Some(dir) => dir.canonicalize().unwrap(),
        None => std::env::current_dir().unwrap(),
    };

    let usage = get_usage(&dir, 0, &config, &running_config)
        .ok_or_else(|| format!("Failed to get usage"))?;

    print_tree(&usage)
        .map_err(|err| format!("Failed to print the tree: {}", err))?;

    Ok(())
}

fn main() {
    if let Err(err) = run() {
        eprintln!("{}", err);
    }
}
