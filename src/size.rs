use crate::config::Config;

pub fn adjust_size(size: u64) -> String {
    let unit = if size < 1024 {
        "B"
    } else if size < 1024u64.pow(2) {
        "KB"
    } else if size < 1024u64.pow(3) {
        "MB"
    } else {
        "GB"
    };

    let adjusted_size = match unit {
        "B" => size as f64,
        "KB" => size as f64 / 1024.0,
        "MB" => size as f64 / 1024f64.powf(2.0),
        "GB" => size as f64 / 1024f64.powf(3.0),
        _ => panic!("Invalid size"),
    };

    let size_str = match unit {
        "B" => format!("{}", adjusted_size),
        _ => format!("{:.2}", adjusted_size),
    };

    format!("{} {}", size_str, unit)
}

pub fn format_size(size: u64, config: &Config) -> String {
    let first_bound = config
        .coloring_rules
        .iter()
        .filter(|(b, _)| *b > size)
        .map(|(_, c)| c)
        .nth(0);

    let adjusted_size = adjust_size(size);

    if let Some(colour) = first_bound {
        colour.paint(adjusted_size).to_string()
    } else {
        adjusted_size
    }
}

#[test]
fn size_format_tests() {
    assert_eq!(&adjust_size(123), "123 B");
    assert_eq!(&adjust_size(1024), "1.00 KB");
    assert_eq!(&adjust_size(2048), "2.00 KB");
    assert_eq!(&adjust_size(1536), "1.50 KB");
    assert_eq!(&adjust_size(1024u64.pow(2)), "1.00 MB");
    assert_eq!(&adjust_size(1024u64.pow(3)), "1.00 GB");
}
